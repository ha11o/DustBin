import pickle

def serialize(file,data):
    fl  = open(file+'','wb')
    pickle.dump(data,fl,protocol=pickle.HIGHEST_PROTOCOL)
    fl.close()
    return


def deserialize(file):
    fl = open(file,'rb')
    data = pickle.load(fl)
    fl.close()
    return data
