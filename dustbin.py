#enoder and decoder finctions here

import math

def makeDiff(ls):
    new_ls = [ls[0]]
    max_in_diff = -1
    for each in range(1,len(ls)):
        diff = ls[each]-ls[each-1]
        new_ls.append(diff)
    return new_ls

def compressList(color):
    new_color = {}
    for i in range(0,256):
        ls  =  color[i]
        if len(ls)>0:
            diff_list = makeDiff(ls)
            new_color[i] = diff_list

    return new_color

def decompress(ls):
    value_one = ls[0]
    decompressed_ls = []
    for i in range(1,len(ls)):
        decompressed_ls.append(value_one+ls[i])
        value_one = ls[i]
    return decompressed_ls


def helper_decoder(color):
    original_color = {}
    for each in range(0,256):
        compressed_str =  color[each]
        original_color[each] = decompress(compressed_str)
    return original_color




def Decode(r,g,b):
    return helper_decoder(r),helper_decoder(g),helper_decoder(b)

def Encode(r,g,b):
    return compressList(r),compressList(g),compressList(b)
