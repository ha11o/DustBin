from PIL import Image
import numpy as np
import argparse
import glob
import logging
import os
import dustbin
import dump
import shutil

#this sucks
logging.basicConfig(level=logging.DEBUG)

try:
    os.mkdir('compressed_files')
except:
    print('Directlory already exists')


parser = argparse.ArgumentParser()
parser.add_argument("dataset")
parser.add_argument("output")
args = parser.parse_args()

data_folder = args.dataset
output_file = args.output

logging.info('Getting all the files in folder'+data_folder)

all_files = glob.glob(data_folder+'/*.*')


for each_image in all_files:
    try:
        img =  Image.open(each_image).convert('L')
        #get the rgb values
        rgb_img = img.convert('RGB')

        max_x,max_y = img.size

        posting_img = {}
        for i in range(max_x):
            r,g,b = {},{},{}
            colors = [r,g,b]

            #make the empty posting list for each of the vaues of r,g,b
            for each_color in colors:
                for all_values_color in range(0,256):
                    each_color[all_values_color] = []

            for j in range(max_y):
                r_value,g_value,b_value = rgb_img.getpixel((i,j))
                r[r_value].append(j)
                g[g_value].append(j)
                b[b_value].append(j)

            #compress the lists
            r,g,b = dustbin.Encode(r,g,b)

            each_posting = {'r':r,'g':g,'b':b}
            posting_img[i] = each_posting

        file_name = each_image.split('.')
        dump.serialize('compressed_files/'+file_name[0].split('/')[1]+'.pickle',posting_img)
        posting_img.clear()

        logging.info(' Read file succesfully ' + each_image)
    except:
        logging.info(' cannot read file '+ each_image)

shutil.make_archive(output_file, 'bztar', 'compressed_files')

print ("from : ",os.stat(data_folder).st_size," to ", os.stat(output_file+'.tar.bz2').st_size)
