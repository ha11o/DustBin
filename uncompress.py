from PIL import Image
import numpy as np
import argparse
import glob
import logging
import os
import dustbin
import dump
import shutil
import tarfile

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument("compressed_folder")
parser.add_argument("output")
args = parser.parse_args()


output = args.output
comp_folder = args.compressed_folder

try:
    os.mkdir(output)
except:
    print('Directlory already exists')


try:
    tar = tarfile.open(comp_folder, "r:bz2")
    tar.extractall(path='unziped_folder')
    tar.close()
except:
    logging.info('Error reading a file')


data_folder = 'unziped_folder'
all_files = glob.glob(data_folder+'/*.*')

for each in all_files:
    data = dump.deserialize(each)
    print (data)
    
